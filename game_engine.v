module game_engine(
  inout PS2_DATA,
  inout PS2_CLK,
  input clk,
  input rst,
  input q_mode,
  input left_signal,
  input right_signal,
  input ran,
  input [9:0] h_cnt,
  input [9:0] v_cnt,
  input valid,
  input [9:0] length,
  output reg [3:0] vgaRed,
  output reg [3:0] vgaGreen,
  output reg [3:0] vgaBlue,
  output reg [2:0] cur_flag,
  output [3:0] sel,
  output [6:0] display,
  output reg [9:0] reward,
  output reg [9:0] ball_x,
  output reg [9:0] ball_y,
  output reg [49:0] map,
  output reg [2:0] game_state = 3'd0  // 0 => initial , 1 => lose , 2 => win
);

`define grey 12'h888
`define red 12'hf11
`define orange 12'hf53
`define yellow 12'hff5
`define green 12'h6c2
`define blue 12'h5ce
`define purple 12'ha0a
`define black 12'h000
`define silver 12'hbbb

`define A 8'h1C  // left
`define D 8'h23  // right
`define W 8'h1D  // up
`define X 8'h22  // down
`define space 8'h29   // space

`define thickness 10'd8
`define radius 10'd3

`define velocity 10'd25

// 10a+2b+9c = 600
`define a 10'd58
`define b 10'd1
`define c 10'd2

`define init 5'd0
`define gaming 5'd1
`define stop 5'd2

reg [9:0] vx = 10'd1;
reg [9:0] vy = 10'd1;
reg [9:0] ball_x_nxt,ball_y_nxt;
reg [9:0] ball_right,ball_left,ball_top,ball_bottom;
reg [5:0] state , nxt_state;
reg [2:0] game_state_nxt; 
wire [49:0] map_tmp;
reg [49:0] map_nxt;  // 640 * 480
reg [6:0] score,score_nxt;
wire clk_60hz,clk_fpga;
wire [6:0] tens,digits;
reg [6:0] num;
wire [511:0] key_down;
wire [8:0] last_change;
wire key_valid;
wire [7:0] makecode;
reg [9:0] h_control = 10'd320;
reg [9:0] v_control = 10'd440;

reg [9:0] reward_nxt;

wire clk_1sec;

Random r(clk,map_tmp);
freq_div_60hz f(clk,clk_60hz);
Freq_div_60hz f2(clk,clk_fpga);
show_score s(score,tens,digits);
sev_segment sev(num,display);
Freq_div_1hz f3(clk,clk_1sec);

DFF D(clk_fpga,sel);
wire [49:0] tmp;
assign tmp = map_tmp&50'h3fffffff00000;

reg flag_1_nxt;
reg flag_2_nxt;
reg flag_3_nxt;
reg flag_4_nxt;
reg flag_1;
reg flag_2;
reg flag_3;
reg flag_4;
reg [2:0]  cur_flag_nxt; 
reg nxt_lock = 1'b0;
reg lock_delay = 1'b0;
reg lock = 1'b0;
reg [9:0] h_control_nxt = 10'd320;
reg [9:0] v_control_nxt  = 10'd440;
reg signal_left,signal_right;
reg signal_left_nxt,signal_right_nxt;

assign makecode = {last_change[7:0]};

KeyboardDecoder key_ctrl(
	.key_down(key_down),
	.last_change(last_change),
	.key_valid(key_valid),
	.PS2_DATA(PS2_DATA),
	.PS2_CLK(PS2_CLK),
	.rst(rst),
	.clk(clk)
);

always@(*)begin
  case(sel)
  4'b1110:num = digits;
  4'b1101:num = tens;
  4'b1011:num = 7'd0;
  4'b0111:num = 7'd0;
  default:num = 7'd0;
  endcase
end

always@(*)begin
  ball_left = ball_x - `radius / 10'd2;
  ball_right = ball_x + `radius / 10'd2;
  ball_top = ball_y - `radius / 10'd2;
  ball_bottom = ball_y + `radius / 10'd2;
end

// draw screen 
always @(*) begin
  if(!valid) {vgaRed, vgaGreen, vgaBlue} = 12'h0;
  else if(v_cnt <= 10'd20 || v_cnt >= 10'd460 || h_cnt <= 10'd20 || h_cnt >= 10'd620)  {vgaRed, vgaGreen, vgaBlue} = `grey;   // frame
  else if(h_cnt <= h_control + length / 10'd2 && h_cnt >= h_control - length / 10'd2
  && v_cnt <= v_control + `thickness / 10'd2 && v_cnt >= v_control-`thickness / 10'd2) {vgaRed, vgaGreen, vgaBlue} = `purple; // plane

  else if(h_cnt <= ball_x + `radius / 10'd2 && h_cnt >= ball_x - `radius / 10'd2
  && v_cnt <= ball_y + `radius / 10'd2 && v_cnt >= ball_y-`radius / 10'd2)  {vgaRed, vgaGreen, vgaBlue} = `black; // ball
  else if(map[49] == 1'b1 && (h_cnt >= 10'd20 + `b && h_cnt < 10'd20 +`a +`b && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[48] == 1'b1 && (h_cnt >= 10'd20 + `a + `b + `c && h_cnt < 10'd20 + 2*`a+`b+`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[47] == 1'b1 && (h_cnt >= 10'd20 + 2*`a+`b+2*`c && h_cnt < 10'd20 + 3*`a+`b+2*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[46] == 1'b1 && (h_cnt >= 10'd20 + 3*`a+`b+3*`c && h_cnt < 10'd20 + 4*`a+`b+3*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[45] == 1'b1 && (h_cnt >= 10'd20 + 4*`a+`b+4*`c && h_cnt < 10'd20 + 5*`a+`b+4*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[44] == 1'b1 && (h_cnt >= 10'd20 + 5*`a+`b+5*`c && h_cnt < 10'd20 + 6*`a+`b+5*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[43] == 1'b1 && (h_cnt >= 10'd20 + 6*`a+`b+6*`c && h_cnt < 10'd20 + 7*`a+`b+6*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[42] == 1'b1 && (h_cnt >= 10'd20 + 7*`a+`b+7*`c && h_cnt < 10'd20 + 8*`a+`b+7*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[41] == 1'b1 && (h_cnt >= 10'd20 + 8*`a+`b+8*`c && h_cnt < 10'd20 + 9*`a+`b+8*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;
  else if(map[40] == 1'b1 && (h_cnt >= 10'd20 + 9*`a+`b+9*`c && h_cnt < 10'd20 + 10*`a+`b+9*`c && v_cnt >= 10'd30 && v_cnt < 10'd50 ))  {vgaRed, vgaGreen, vgaBlue} = `red;

  else if(map[39] == 1'b1 && (h_cnt >= 10'd20 + `b && h_cnt < 10'd20 +`a +`b && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[38] == 1'b1 && (h_cnt >= 10'd20 + `a + `b + `c && h_cnt < 10'd20 + 2*`a+`b+`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[37] == 1'b1 && (h_cnt >= 10'd20 + 2*`a+`b+2*`c && h_cnt < 10'd20 + 3*`a+`b+2*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[36] == 1'b1 && (h_cnt >= 10'd20 + 3*`a+`b+3*`c && h_cnt < 10'd20 + 4*`a+`b+3*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[35] == 1'b1 && (h_cnt >= 10'd20 + 4*`a+`b+4*`c && h_cnt < 10'd20 + 5*`a+`b+4*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[34] == 1'b1 && (h_cnt >= 10'd20 + 5*`a+`b+5*`c && h_cnt < 10'd20 + 6*`a+`b+5*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[33] == 1'b1 && (h_cnt >= 10'd20 + 6*`a+`b+6*`c && h_cnt < 10'd20 + 7*`a+`b+6*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[32] == 1'b1 && (h_cnt >= 10'd20 + 7*`a+`b+7*`c && h_cnt < 10'd20 + 8*`a+`b+7*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[31] == 1'b1 && (h_cnt >= 10'd20 + 8*`a+`b+8*`c && h_cnt < 10'd20 + 9*`a+`b+8*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;
  else if(map[30] == 1'b1 && (h_cnt >= 10'd20 + 9*`a+`b+9*`c && h_cnt < 10'd20 + 10*`a+`b+9*`c && v_cnt >= 10'd52 && v_cnt < 10'd72 ))  {vgaRed, vgaGreen, vgaBlue} = `orange;

  else if(map[29] == 1'b1 && (h_cnt >= 10'd20 + `b && h_cnt < 10'd20 +`a +`b && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[28] == 1'b1 && (h_cnt >= 10'd20 + `a + `b + `c && h_cnt < 10'd20 + 2*`a+`b+`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[27] == 1'b1 && (h_cnt >= 10'd20 + 2*`a+`b+2*`c && h_cnt < 10'd20 + 3*`a+`b+2*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[26] == 1'b1 && (h_cnt >= 10'd20 + 3*`a+`b+3*`c && h_cnt < 10'd20 + 4*`a+`b+3*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[25] == 1'b1 && (h_cnt >= 10'd20 + 4*`a+`b+4*`c && h_cnt < 10'd20 + 5*`a+`b+4*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[24] == 1'b1 && (h_cnt >= 10'd20 + 5*`a+`b+5*`c && h_cnt < 10'd20 + 6*`a+`b+5*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[23] == 1'b1 && (h_cnt >= 10'd20 + 6*`a+`b+6*`c && h_cnt < 10'd20 + 7*`a+`b+6*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[22] == 1'b1 && (h_cnt >= 10'd20 + 7*`a+`b+7*`c && h_cnt < 10'd20 + 8*`a+`b+7*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[21] == 1'b1 && (h_cnt >= 10'd20 + 8*`a+`b+8*`c && h_cnt < 10'd20 + 9*`a+`b+8*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;
  else if(map[20] == 1'b1 && (h_cnt >= 10'd20 + 9*`a+`b+9*`c && h_cnt < 10'd20 + 10*`a+`b+9*`c && v_cnt >= 10'd74 && v_cnt < 10'd94 ))  {vgaRed, vgaGreen, vgaBlue} = `yellow;

  else if(map[19] == 1'b1 && (h_cnt >= 10'd20 + `b && h_cnt < 10'd20 +`a +`b && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[18] == 1'b1 && (h_cnt >= 10'd20 + `a + `b + `c && h_cnt < 10'd20 + 2*`a+`b+`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[17] == 1'b1 && (h_cnt >= 10'd20 + 2*`a+`b+2*`c && h_cnt < 10'd20 + 3*`a+`b+2*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[16] == 1'b1 && (h_cnt >= 10'd20 + 3*`a+`b+3*`c && h_cnt < 10'd20 + 4*`a+`b+3*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[15] == 1'b1 && (h_cnt >= 10'd20 + 4*`a+`b+4*`c && h_cnt < 10'd20 + 5*`a+`b+4*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[14] == 1'b1 && (h_cnt >= 10'd20 + 5*`a+`b+5*`c && h_cnt < 10'd20 + 6*`a+`b+5*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[13] == 1'b1 && (h_cnt >= 10'd20 + 6*`a+`b+6*`c && h_cnt < 10'd20 + 7*`a+`b+6*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[12] == 1'b1 && (h_cnt >= 10'd20 + 7*`a+`b+7*`c && h_cnt < 10'd20 + 8*`a+`b+7*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[11] == 1'b1 && (h_cnt >= 10'd20 + 8*`a+`b+8*`c && h_cnt < 10'd20 + 9*`a+`b+8*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;
  else if(map[10] == 1'b1 && (h_cnt >= 10'd20 + 9*`a+`b+9*`c && h_cnt < 10'd20 + 10*`a+`b+9*`c && v_cnt >= 10'd96 && v_cnt < 10'd116 ))  {vgaRed, vgaGreen, vgaBlue} = `green;

  else if(map[9] == 1'b1 && (h_cnt >= 10'd20 + `b && h_cnt < 10'd20 +`a +`b && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[8] == 1'b1 && (h_cnt >= 10'd20 + `a + `b + `c && h_cnt < 10'd20 + 2*`a+`b+`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[7] == 1'b1 && (h_cnt >= 10'd20 + 2*`a+`b+2*`c && h_cnt < 10'd20 + 3*`a+`b+2*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[6] == 1'b1 && (h_cnt >= 10'd20 + 3*`a+`b+3*`c && h_cnt < 10'd20 + 4*`a+`b+3*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[5] == 1'b1 && (h_cnt >= 10'd20 + 4*`a+`b+4*`c && h_cnt < 10'd20 + 5*`a+`b+4*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[4] == 1'b1 && (h_cnt >= 10'd20 + 5*`a+`b+5*`c && h_cnt < 10'd20 + 6*`a+`b+5*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[3] == 1'b1 && (h_cnt >= 10'd20 + 6*`a+`b+6*`c && h_cnt < 10'd20 + 7*`a+`b+6*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[2] == 1'b1 && (h_cnt >= 10'd20 + 7*`a+`b+7*`c && h_cnt < 10'd20 + 8*`a+`b+7*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[1] == 1'b1 && (h_cnt >= 10'd20 + 8*`a+`b+8*`c && h_cnt < 10'd20 + 9*`a+`b+8*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else if(map[0] == 1'b1 && (h_cnt >= 10'd20 + 9*`a+`b+9*`c && h_cnt < 10'd20 + 10*`a+`b+9*`c && v_cnt >= 10'd118 && v_cnt < 10'd138 ))  {vgaRed, vgaGreen, vgaBlue} = `blue;
  else {vgaRed, vgaGreen, vgaBlue} = 12'hfff;
end

always@(*)begin
  if(cur_flag == 3'd5)begin  // game lose
    flag_1_nxt = 1'b0;
    flag_2_nxt = 1'b0;
    flag_3_nxt = 1'b0;
    flag_4_nxt = 1'b0;
    game_state_nxt = 3'd1;
  end
  else if( (50'h3fffffff00000&map) == 50'd0 )begin   // game win
    flag_1_nxt = 1'b0;
    flag_2_nxt = 1'b0;
    flag_3_nxt = 1'b0;
    flag_4_nxt = 1'b0;
    game_state_nxt = 3'd2;
  end
  else if(cur_flag == 3'd0)begin
      flag_1_nxt = 1'b0;
      flag_2_nxt = 1'b0;
      flag_3_nxt = 1'b0;
      flag_4_nxt = 1'b0;
      game_state_nxt = 3'd0;
  end
  else if(cur_flag == 3'd1)begin
     flag_1_nxt = 1'b1;
      flag_2_nxt = 1'b0;
      flag_3_nxt = 1'b0;
      flag_4_nxt = 1'b0;
      game_state_nxt = 3'd0;
  end
  else if(cur_flag == 3'd2)begin
    flag_1_nxt = 1'b0;
      flag_2_nxt = 1'b1;
      flag_3_nxt = 1'b0;
      flag_4_nxt = 1'b0;
      game_state_nxt = 3'd0;
  end
  else if(cur_flag == 3'd3)begin
    flag_1_nxt = 1'b0;
      flag_2_nxt = 1'b0;
      flag_3_nxt = 1'b1;
      flag_4_nxt = 1'b0;
      game_state_nxt = 3'd0;
  end
  else if(cur_flag == 3'd4)begin
    flag_1_nxt = 1'b0;
      flag_2_nxt = 1'b0;
      flag_3_nxt = 1'b0;
      flag_4_nxt = 1'b1;
      game_state_nxt = 3'd0;
  end
  else begin
      flag_1_nxt = 1'b0;
      flag_2_nxt = 1'b0;
      flag_3_nxt = 1'b0;
      flag_4_nxt = 1'b0;
      game_state_nxt = 3'd0;
  end
end

always@(*)begin
  if(ball_y >= 10'd620)begin
    cur_flag_nxt = 3'd5;
  end
  else if( (ball_top < v_control - `thickness / 10'd2) &&(ball_bottom >= v_control - `thickness / 10'd2) && (ball_x <= h_control + length / 10'd2 && ball_x >= h_control - length / 10'd2))begin   // ball collide with plane
    if(cur_flag == 3'd0 || cur_flag == 3'd4)cur_flag_nxt = 3'd1;
    else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
    else cur_flag_nxt = cur_flag + 3'd0;
  end
  else if( ball_x >= 10'd620 - `radius / 10'd2 )begin
    if(cur_flag == 3'd1 )cur_flag_nxt = 3'd2;
    else if(cur_flag == 3'd4 )cur_flag_nxt = 3'd3;
    else cur_flag_nxt = cur_flag + 3'd0;
  end
  else if(ball_y <= 10'd20 + `radius / 10'd2)begin
    if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
    else if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
    else cur_flag_nxt = cur_flag + 3'd0;
  end
  else if( ball_x <= 10'd20 + `radius / 10'd2)begin
    if(cur_flag == 3'd3)cur_flag_nxt = 3'd4;
    else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
    else cur_flag_nxt = cur_flag + 3'd0;
  end
  else if( map[49] && ( ball_left < 10'd20 + `b && ball_right >= 10'd20 + `b && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[49] && ( ball_right > 10'd20 +`a +`b && ball_left <= 10'd20 +`a +`b && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[49] && (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[49] && (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end
  
  else if( map[48] && ( ball_left < 10'd20 + `a + `b + `c && ball_right >= 10'd20 + `a + `b + `c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[48] && ( ball_right > 10'd20 + 2*`a+`b+`c && ball_left <= 10'd20 + 2*`a+`b+`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[48] && (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[48] && (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[47] && ( ball_left < 10'd20 + 2*`a+`b+2*`c&& ball_right >= 10'd20 + 2*`a+`b+2*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[47] && ( ball_right > 10'd20 + 3*`a+`b+2*`c && ball_left <= 10'd20 + 3*`a+`b+2*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[47] && (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[47] && (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[46] && ( ball_left < 10'd20 + 3*`a+`b+3*`c&& ball_right >= 10'd20 + 3*`a+`b+3*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[46] && ( ball_right > 10'd20 + 4*`a+`b+3*`c && ball_left <= 10'd20 + 4*`a+`b+3*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[46] && (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[46] && (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[45] && ( ball_left < 10'd20 + 4*`a+`b+4*`c&& ball_right >= 10'd20 + 4*`a+`b+4*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[45] && ( ball_right > 10'd20 + 5*`a+`b+4*`c && ball_left <= 10'd20 + 5*`a+`b+4*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[45] && (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[45] && (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end
  
  else if( map[44] && ( ball_left < 10'd20 + 5*`a+`b+5*`c&& ball_right >= 10'd20 + 5*`a+`b+5*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[44] && ( ball_right > 10'd20 + 6*`a+`b+5*`c && ball_left <= 10'd20 + 6*`a+`b+5*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[44] && (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[44] && (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[43] && ( ball_left < 10'd20 + 6*`a+`b+6*`c&& ball_right >= 10'd20 + 6*`a+`b+6*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[43] && ( ball_right > 10'd20 + 7*`a+`b+6*`c && ball_left <= 10'd20 + 7*`a+`b+6*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[43] && (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[43] && (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[42] && ( ball_left < 10'd20 + 7*`a+`b+7*`c&& ball_right >= 10'd20 + 7*`a+`b+7*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[42] && ( ball_right > 10'd20 + 8*`a+`b+7*`c && ball_left <= 10'd20 + 8*`a+`b+7*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[42] && (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[42] && (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[41] && ( ball_left < 10'd20 + 8*`a+`b+8*`c&& ball_right >= 10'd20 + 8*`a+`b+8*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[41] && ( ball_right > 10'd20 + 9*`a+`b+8*`c && ball_left <= 10'd20 + 9*`a+`b+8*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[41] && (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[41] && (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[40] && ( ball_left < 10'd20 + 9*`a+`b+9*`c&& ball_right >= 10'd20 + 9*`a+`b+9*`c && ball_top >= 10'd30 && ball_bottom < 10'd50))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[40] && ( ball_right > 10'd20 + 10*`a+`b+9*`c && ball_left <= 10'd20 + 10*`a+`b+9*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[40] && (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c && ball_bottom >= 10'd30 && ball_top < 10'd30)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[40] && (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[39] && ( ball_left < 10'd20 + `b && ball_right >= 10'd20 + `b && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[39] && ( ball_right > 10'd20 +`a +`b && ball_left <= 10'd20 +`a +`b && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[39] && (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[39] && (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end
  
  else if( map[38] && ( ball_left < 10'd20 + `a + `b + `c && ball_right >= 10'd20 + `a + `b + `c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[38] && ( ball_right > 10'd20 + 2*`a+`b+`c && ball_left <= 10'd20 + 2*`a+`b+`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[38] && (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[38] && (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[37] && ( ball_left < 10'd20 + 2*`a+`b+2*`c&& ball_right >= 10'd20 + 2*`a+`b+2*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[37] && ( ball_right > 10'd20 + 3*`a+`b+2*`c && ball_left <= 10'd20 + 3*`a+`b+2*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[37] && (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[37] && (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[36] && ( ball_left < 10'd20 + 3*`a+`b+3*`c&& ball_right >= 10'd20 + 3*`a+`b+3*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[36] && ( ball_right > 10'd20 + 4*`a+`b+3*`c && ball_left <= 10'd20 + 4*`a+`b+3*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[36] && (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[36] && (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[35] && ( ball_left < 10'd20 + 4*`a+`b+4*`c&& ball_right >= 10'd20 + 4*`a+`b+4*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[35] && ( ball_right > 10'd20 + 5*`a+`b+4*`c && ball_left <= 10'd20 + 5*`a+`b+4*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[35] && (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[35] && (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end
  
  else if( map[34] && ( ball_left < 10'd20 + 5*`a+`b+5*`c&& ball_right >= 10'd20 + 5*`a+`b+5*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[34] && ( ball_right > 10'd20 + 6*`a+`b+5*`c && ball_left <= 10'd20 + 6*`a+`b+5*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[34] && (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[34] && (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[33] && ( ball_left < 10'd20 + 6*`a+`b+6*`c&& ball_right >= 10'd20 + 6*`a+`b+6*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[33] && ( ball_right > 10'd20 + 7*`a+`b+6*`c && ball_left <= 10'd20 + 7*`a+`b+6*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[33] && (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[33] && (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[32] && ( ball_left < 10'd20 + 7*`a+`b+7*`c&& ball_right >= 10'd20 + 7*`a+`b+7*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[32] && ( ball_right > 10'd20 + 8*`a+`b+7*`c && ball_left <= 10'd20 + 8*`a+`b+7*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[32] && (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[32] && (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[31] && ( ball_left < 10'd20 + 8*`a+`b+8*`c&& ball_right >= 10'd20 + 8*`a+`b+8*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[31] && ( ball_right > 10'd20 + 9*`a+`b+8*`c && ball_left <= 10'd20 + 9*`a+`b+8*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[31] && (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[31] && (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[30] && ( ball_left < 10'd20 + 9*`a+`b+9*`c&& ball_right >= 10'd20 + 9*`a+`b+9*`c && ball_top >= 10'd52 && ball_bottom < 10'd72))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[30] && ( ball_right > 10'd20 + 10*`a+`b+9*`c && ball_left <= 10'd20 + 10*`a+`b+9*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[30] && (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c && ball_bottom >= 10'd52 && ball_top < 10'd52)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[30] && (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[29] && ( ball_left < 10'd20 + `b && ball_right >= 10'd20 + `b && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[29] && ( ball_right > 10'd20 +`a +`b && ball_left <= 10'd20 +`a +`b && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[29] && (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[29] && (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end
  
  else if( map[28] && ( ball_left < 10'd20 + `a + `b + `c && ball_right >= 10'd20 + `a + `b + `c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[28] && ( ball_right > 10'd20 + 2*`a+`b+`c && ball_left <= 10'd20 + 2*`a+`b+`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[28] && (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[28] && (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[27] && ( ball_left < 10'd20 + 2*`a+`b+2*`c&& ball_right >= 10'd20 + 2*`a+`b+2*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[27] && ( ball_right > 10'd20 + 3*`a+`b+2*`c && ball_left <= 10'd20 + 3*`a+`b+2*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[27] && (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[27] && (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94 ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[26] && ( ball_left < 10'd20 + 3*`a+`b+3*`c&& ball_right >= 10'd20 + 3*`a+`b+3*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[26] && ( ball_right > 10'd20 + 4*`a+`b+3*`c && ball_left <= 10'd20 + 4*`a+`b+3*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[26] && (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[26] && (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[25] && ( ball_left < 10'd20 + 4*`a+`b+4*`c&& ball_right >= 10'd20 + 4*`a+`b+4*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[25] && ( ball_right > 10'd20 + 5*`a+`b+4*`c && ball_left <= 10'd20 + 5*`a+`b+4*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[25] && (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[25] && (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end
  
  else if( map[24] && ( ball_left < 10'd20 + 5*`a+`b+5*`c&& ball_right >= 10'd20 + 5*`a+`b+5*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[24] && ( ball_right > 10'd20 + 6*`a+`b+5*`c && ball_left <= 10'd20 + 6*`a+`b+5*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[24] && (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[24] && (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[23] && ( ball_left < 10'd20 + 6*`a+`b+6*`c&& ball_right >= 10'd20 + 6*`a+`b+6*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[23] && ( ball_right > 10'd20 + 7*`a+`b+6*`c && ball_left <= 10'd20 + 7*`a+`b+6*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[23] && (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[23] && (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[22] && ( ball_left < 10'd20 + 7*`a+`b+7*`c&& ball_right >= 10'd20 + 7*`a+`b+7*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[22] && ( ball_right > 10'd20 + 8*`a+`b+7*`c && ball_left <= 10'd20 + 8*`a+`b+7*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[22] && (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[22] && (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[21] && ( ball_left < 10'd20 + 8*`a+`b+8*`c&& ball_right >= 10'd20 + 8*`a+`b+8*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[21] && ( ball_right > 10'd20 + 9*`a+`b+8*`c && ball_left <= 10'd20 + 9*`a+`b+8*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[21] && (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[21] && (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end

  else if( map[20] && ( ball_left < 10'd20 + 9*`a+`b+9*`c&& ball_right >= 10'd20 + 9*`a+`b+9*`c && ball_top >= 10'd74 && ball_bottom < 10'd94))begin  // �����J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd3;
      else if(cur_flag == 3'd1)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[20] && ( ball_right > 10'd20 + 10*`a+`b+9*`c && ball_left <= 10'd20 + 10*`a+`b+9*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) )begin // �k���J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd1;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[20] && (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c && ball_bottom >= 10'd74 && ball_top < 10'd74)  )begin // �W��J�g
      if(cur_flag == 3'd4)cur_flag_nxt = 3'd1;
      else if(cur_flag == 3'd3)cur_flag_nxt = 3'd2;
      else cur_flag_nxt = cur_flag;
  end
  else if( map[20] && (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) )begin  // �U��J�g
      if(cur_flag == 3'd1)cur_flag_nxt = 3'd4;
      else if(cur_flag == 3'd2)cur_flag_nxt = 3'd3;
      else cur_flag_nxt = cur_flag;
  end
  else begin
    cur_flag_nxt = cur_flag;
  end
end

always@(*)begin
  if( map[49] && (  
  ( ball_left < 10'd20 + `b && ball_right >= 10'd20 + `b && ball_top >= 10'd30 && ball_bottom < 10'd50)||
  ( ball_right > 10'd20 +`a +`b && ball_left <= 10'd20 +`a +`b && ball_top >= 10'd30 && ball_bottom < 10'd50 )||
  (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {1'b0,map[48:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[48] && (
  ( ball_left < 10'd20 + `a + `b + `c && ball_right >= 10'd20 + `a + `b + `c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 + 2*`a+`b+`c && ball_left <= 10'd20 + 2*`a+`b+`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  ) 
  ))begin
    map_nxt <= {map[49],1'b0,map[47:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[47] && (
  ( ball_left < 10'd20 + 2*`a+`b+2*`c&& ball_right >= 10'd20 + 2*`a+`b+2*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 + 3*`a+`b+2*`c && ball_left <= 10'd20 + 3*`a+`b+2*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:48],1'b0,map[46:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[46] && (
  ( ball_left < 10'd20 + 3*`a+`b+3*`c&& ball_right >= 10'd20 + 3*`a+`b+3*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 + 4*`a+`b+3*`c && ball_left <= 10'd20 + 4*`a+`b+3*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:47],1'b0,map[45:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[45] && (
  ( ball_left < 10'd20 + 4*`a+`b+4*`c&& ball_right >= 10'd20 + 4*`a+`b+4*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 + 5*`a+`b+4*`c && ball_left <= 10'd20 + 5*`a+`b+4*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:46],1'b0,map[44:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[44] && (
  ( ball_left < 10'd20 + 5*`a+`b+5*`c&& ball_right >= 10'd20 + 5*`a+`b+5*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 + 6*`a+`b+5*`c && ball_left <= 10'd20 + 6*`a+`b+5*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:45],1'b0,map[43:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[43] && (
  ( ball_left < 10'd20 + 6*`a+`b+6*`c&& ball_right >= 10'd20 + 6*`a+`b+6*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 + 7*`a+`b+6*`c && ball_left <= 10'd20 + 7*`a+`b+6*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:44],1'b0,map[42:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[42] && (
  ( ball_left < 10'd20 + 7*`a+`b+7*`c&& ball_right >= 10'd20 + 7*`a+`b+7*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 + 8*`a+`b+7*`c && ball_left <= 10'd20 + 8*`a+`b+7*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:43],1'b0,map[41:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[41] && (
  ( ball_left < 10'd20 + 8*`a+`b+8*`c&& ball_right >= 10'd20 + 8*`a+`b+8*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20 +9*`a+`b+8*`c && ball_left <= 10'd20 + 9*`a+`b+8*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:42],1'b0,map[40:0]};
    score_nxt = score + 7'd3;
  end
  else if(map[40] && (
  ( ball_left < 10'd20 + 9*`a+`b+9*`c&& ball_right >= 10'd20 + 9*`a+`b+9*`c && ball_top >= 10'd30 && ball_bottom < 10'd50) ||
  ( ball_right > 10'd20+10*`a+`b+9*`c && ball_left <= 10'd20 + 10*`a+`b+9*`c && ball_top >= 10'd30 && ball_bottom < 10'd50 ) ||
  (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c && ball_bottom >= 10'd30 && ball_top < 10'd30) ||
  (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c  && ball_top <= 10'd50 && ball_bottom > 10'd50  )
  ))begin
    map_nxt <= {map[49:41],1'b0,map[39:0]};
    score_nxt = score + 7'd3;
  end
  else if( map[39] && (  
  ( ball_left < 10'd20 + `b && ball_right >= 10'd20 + `b && ball_top >= 10'd52 && ball_bottom < 10'd72)||
  ( ball_right > 10'd20 +`a +`b && ball_left <= 10'd20 +`a +`b && ball_top >= 10'd52 && ball_bottom < 10'd72 )||
  (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:40],1'b0,map[38:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[38] && (
  ( ball_left < 10'd20 + `a + `b + `c && ball_right >= 10'd20 + `a + `b + `c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 + 2*`a+`b+`c && ball_left <= 10'd20 + 2*`a+`b+`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  ) 
  ))begin
    map_nxt <= {map[49:39],1'b0,map[37:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[37] && (
  ( ball_left < 10'd20 + 2*`a+`b+2*`c&& ball_right >= 10'd20 + 2*`a+`b+2*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 + 3*`a+`b+2*`c && ball_left <= 10'd20 + 3*`a+`b+2*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:38],1'b0,map[36:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[36] && (
  ( ball_left < 10'd20 + 3*`a+`b+3*`c&& ball_right >= 10'd20 + 3*`a+`b+3*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 + 4*`a+`b+3*`c && ball_left <= 10'd20 + 4*`a+`b+3*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:37],1'b0,map[35:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[35] && (
  ( ball_left < 10'd20 + 4*`a+`b+4*`c&& ball_right >= 10'd20 + 4*`a+`b+4*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 + 5*`a+`b+4*`c && ball_left <= 10'd20 + 5*`a+`b+4*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:36],1'b0,map[34:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[34] && (
  ( ball_left < 10'd20 + 5*`a+`b+5*`c&& ball_right >= 10'd20 + 5*`a+`b+5*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 + 6*`a+`b+5*`c && ball_left <= 10'd20 + 6*`a+`b+5*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:35],1'b0,map[33:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[33] && (
  ( ball_left < 10'd20 + 6*`a+`b+6*`c&& ball_right >= 10'd20 + 6*`a+`b+6*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 + 7*`a+`b+6*`c && ball_left <= 10'd20 + 7*`a+`b+6*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:34],1'b0,map[32:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[32] && (
  ( ball_left < 10'd20 + 7*`a+`b+7*`c&& ball_right >= 10'd20 + 7*`a+`b+7*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 + 8*`a+`b+7*`c && ball_left <= 10'd20 + 8*`a+`b+7*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:33],1'b0,map[31:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[31] && (
  ( ball_left < 10'd20 + 8*`a+`b+8*`c&& ball_right >= 10'd20 + 8*`a+`b+8*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20 +9*`a+`b+8*`c && ball_left <= 10'd20 + 9*`a+`b+8*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:32],1'b0,map[30:0]};
    score_nxt = score + 7'd2;
  end
  else if(map[30] && (
  ( ball_left < 10'd20 + 9*`a+`b+9*`c&& ball_right >= 10'd20 + 9*`a+`b+9*`c && ball_top >= 10'd52 && ball_bottom < 10'd72) ||
  ( ball_right > 10'd20+10*`a+`b+9*`c && ball_left <= 10'd20 + 10*`a+`b+9*`c && ball_top >= 10'd52 && ball_bottom < 10'd72 ) ||
  (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c && ball_bottom >= 10'd52 && ball_top < 10'd52) ||
  (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c  && ball_top <= 10'd72 && ball_bottom > 10'd72  )
  ))begin
    map_nxt <= {map[49:31],1'b0,map[29:0]};
    score_nxt = score + 7'd2;
  end
  else if( map[29] && (  
  ( ball_left < 10'd20 + `b && ball_right >= 10'd20 + `b && ball_top >= 10'd74 && ball_bottom < 10'd94)||
  ( ball_right > 10'd20 +`a +`b && ball_left <= 10'd20 +`a +`b && ball_top >= 10'd74 && ball_bottom < 10'd94 )||
  (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + `b && ball_right < 10'd20 +`a +`b  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:30],1'b0,map[28:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[28] && (
  ( ball_left < 10'd20 + `a + `b + `c && ball_right >= 10'd20 + `a + `b + `c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20 + 2*`a+`b+`c && ball_left <= 10'd20 + 2*`a+`b+`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + `a + `b + `c && ball_right < 10'd20 + 2*`a+`b+`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  ) 
  ))begin
    map_nxt <= {map[49:29],1'b0,map[27:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[27] && (
  ( ball_left < 10'd20 + 2*`a+`b+2*`c&& ball_right >= 10'd20 + 2*`a+`b+2*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20 + 3*`a+`b+2*`c && ball_left <= 10'd20 + 3*`a+`b+2*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 2*`a+`b+2*`c && ball_right < 10'd20 + 3*`a+`b+2*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:28],1'b0,map[26:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[26] && (
  ( ball_left < 10'd20 + 3*`a+`b+3*`c&& ball_right >= 10'd20 + 3*`a+`b+3*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20 + 4*`a+`b+3*`c && ball_left <= 10'd20 + 4*`a+`b+3*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 3*`a+`b+3*`c && ball_right < 10'd20 + 4*`a+`b+3*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:27],1'b0,map[25:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[25] && (
  ( ball_left < 10'd20 + 4*`a+`b+4*`c&& ball_right >= 10'd20 + 4*`a+`b+4*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20 + 5*`a+`b+4*`c && ball_left <= 10'd20 + 5*`a+`b+4*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 4*`a+`b+4*`c && ball_right < 10'd20 + 5*`a+`b+4*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:26],1'b0,map[24:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[24] && (
  ( ball_left < 10'd20 + 5*`a+`b+5*`c&& ball_right >= 10'd20 + 5*`a+`b+5*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20 + 6*`a+`b+5*`c && ball_left <= 10'd20 + 6*`a+`b+5*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 5*`a+`b+5*`c && ball_right < 10'd20 + 6*`a+`b+5*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:25],1'b0,map[23:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[23] && (
  ( ball_left < 10'd20 + 6*`a+`b+6*`c&& ball_right >= 10'd20 + 6*`a+`b+6*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20 + 7*`a+`b+6*`c && ball_left <= 10'd20 + 7*`a+`b+6*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 6*`a+`b+6*`c && ball_right < 10'd20 + 7*`a+`b+6*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:24],1'b0,map[22:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[22] && (
  ( ball_left < 10'd20 + 7*`a+`b+7*`c&& ball_right >= 10'd20 + 7*`a+`b+7*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20 + 8*`a+`b+7*`c && ball_left <= 10'd20 + 8*`a+`b+7*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 7*`a+`b+7*`c && ball_right < 10'd20 + 8*`a+`b+7*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:23],1'b0,map[21:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[21] && (
  (ball_left < 10'd20 + 8*`a+`b+8*`c&& ball_right >= 10'd20 + 8*`a+`b+8*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  (ball_right > 10'd20 +9*`a+`b+8*`c && ball_left <= 10'd20 + 9*`a+`b+8*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 8*`a+`b+8*`c && ball_right < 10'd20 + 9*`a+`b+8*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:22],1'b0,map[20:0]};
    score_nxt = score + 7'd1;
  end
  else if(map[20] && (
  ( ball_left < 10'd20 + 9*`a+`b+9*`c&& ball_right >= 10'd20 + 9*`a+`b+9*`c && ball_top >= 10'd74 && ball_bottom < 10'd94) ||
  ( ball_right > 10'd20+10*`a+`b+9*`c && ball_left <= 10'd20 + 10*`a+`b+9*`c && ball_top >= 10'd74 && ball_bottom < 10'd94 ) ||
  (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c && ball_bottom >= 10'd74 && ball_top < 10'd74) ||
  (ball_left >= 10'd20 + 9*`a+`b+9*`c && ball_right < 10'd20 + 10*`a+`b+9*`c  && ball_top <= 10'd94 && ball_bottom > 10'd94  )
  ))begin
    map_nxt <= {map[49:21],1'b0,map[19:0]};
    score_nxt = score + 7'd1;
  end
  else begin
    score_nxt = score + 7'd0;
    map_nxt <= map;
  end
end

always@(posedge clk)begin
  if(rst == 1'b1 || game_state > 3'd0)begin
    lock <= 1'b0;
    lock_delay <= 1'b0;
    state <= `init;
    cur_flag <= 3'd0;
    flag_1 <= 1'b0;
    flag_2 <= 1'b0;
    flag_3 <= 1'b0;
    flag_4 <= 1'b0;
    game_state <= 3'd0;
    ball_x <= h_control;
    ball_y <= v_control - `radius / 10'd2 - `thickness / 10'd2;
    signal_left <= 1'b0;
    signal_right <= 1'b0;
    //map = ( ran == 1'b1 ? (map_tmp&50'h3fffffff00000):50'h3fffffff00000);
    if(ran == 1'b1) map =tmp;
    else map = 50'h3fffffff00000;
    score <= 7'd0;
  end
  else begin
    state <= nxt_state;
    cur_flag <= cur_flag_nxt;
    flag_1 <= flag_1_nxt;
    flag_2 <= flag_2_nxt;
    flag_3 <= flag_3_nxt;
    flag_4 <= flag_4_nxt;
    game_state <= game_state_nxt;
    ball_x <= ball_x_nxt;
    ball_y <= ball_y_nxt;
    signal_left <= signal_left_nxt;
    signal_right <= signal_right_nxt;
    map <= map_nxt;
    score <= score_nxt;
    lock <= nxt_lock;
    lock_delay <= lock;
  end
end

always@(posedge clk)begin
  if(rst == 1'b1)begin
    h_control <= 10'd320;
    v_control <= 10'd440;
  end
  else begin
    if(h_control < length / 10'd2 + 10'd20 )begin
      h_control <= length / 10'd2 + 10'd20;
    end
    else if(h_control > 10'd620 - length / 10'd2)begin
      h_control <= 10'd620 - length / 10'd2;
    end
    else begin
      h_control <= h_control_nxt ;
    end
    v_control <= v_control_nxt;
  end
end



always@(*)begin
  if(flag_1 == 1'b1 && clk_60hz == 1'b1)begin   // reflection: /*
    ball_x_nxt = ball_x + vx;
    ball_y_nxt = ball_y - vy; 
  end
  else if(flag_2 == 1'b1 && clk_60hz == 1'b1)begin  // reflection: *\
    ball_x_nxt = ball_x - vx;
    ball_y_nxt = ball_y - vy;
  end
  else if(flag_3 == 1'b1  && clk_60hz == 1'b1)begin  // reflection: ./
    ball_x_nxt = ball_x - vx;
    ball_y_nxt = ball_y + vy;
  end
  else if(flag_4 == 1'b1  && clk_60hz == 1'b1)begin  // reflection: \.
    ball_x_nxt = ball_x + vx;
    ball_y_nxt = ball_y + vy;
  end
  else begin
    ball_x_nxt = ball_x + 10'd0;
    ball_y_nxt = ball_y + 10'd0;
  end
end

always@(*)begin
  if(key_valid == 1'b1 && key_down[last_change] == 1'b1 )begin
    if(makecode == `A && lock_delay == 1'b0)begin
      signal_left_nxt = 1'b1;
      signal_right_nxt = 1'b0;
    end
    else if(makecode == `D && lock_delay == 1'b0)begin
      signal_left_nxt = 1'b0;
      signal_right_nxt = 1'b1;
    end
    else begin
      signal_left_nxt = 1'b0;
      signal_right_nxt = 1'b0;
    end
  end
  else begin
    signal_left_nxt = 1'b0;
    signal_right_nxt = 1'b0;
  end
end

reg game_control_left;
reg game_control_right;

always@(*)begin
  if(q_mode == 1'b1)begin
    game_control_left = left_signal;
    game_control_right = right_signal;
  end
  else begin
    game_control_left = signal_left;
    game_control_right = signal_right;
  end
end

always@(*)begin
  if(game_control_left == 1'b1)begin
    h_control_nxt = h_control - `velocity;
  end
  else if(game_control_right == 1'b1)begin
    h_control_nxt = h_control + `velocity;
  end
  else begin
    h_control_nxt = h_control + 10'd0;
  end
  v_control_nxt = v_control + 10'd0;
end

always@(*)begin
    if(key_valid == 1'b1 && key_down[last_change] == 1'b1 )begin 
        nxt_lock = 1'b1;
    end
    else if(key_valid == 1'b1 && key_down[last_change] == 1'b0 )begin
        nxt_lock = 1'b0;
    end 
    else begin
        nxt_lock = lock + 1'b0;
    end
end


endmodule
