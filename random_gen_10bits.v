`timescale 1ps/1ps

module Random_10bits(
    input clk,
    output reg [9:0] random =10'd200
);

reg [9:0] nxt_random;
wire tmp;

xor Xor(tmp,random[3],random[1],random[7]);

always@(*)begin
    nxt_random = {tmp,random[9:1]};
end

always@(posedge clk)begin
    random <= nxt_random;
end

endmodule