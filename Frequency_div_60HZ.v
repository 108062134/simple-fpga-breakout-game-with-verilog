`timescale 1ps/1ps

module freq_div_60hz(
    input clk,
    output reg div_clk
);
reg [26:0] cnt = 27'd0;
reg [26:0] tmp_cnt;

always@(posedge clk)begin
    if(cnt == 27'd833333)begin
        div_clk <= 1'b1;
    end
    else begin
        div_clk <= 1'b0;
    end
end

always@*
tmp_cnt = cnt+27'd1;

always@(posedge clk)begin
    if(cnt == 27'd833333)begin
        cnt <= 27'd0;
    end
    else begin
        cnt <= tmp_cnt;
    end
    
end

endmodule