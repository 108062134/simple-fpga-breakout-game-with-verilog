`timescale 1ps/1ps

module DFF(
    input clk,
    output reg [3:0] out = 4'b1110
);
always@(posedge clk)begin
    out = {out[2:0],out[3]};
end

endmodule