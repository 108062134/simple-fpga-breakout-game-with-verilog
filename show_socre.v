`timescale 1ps/1ps

module show_score(
    input [6:0]in,
    output reg [6:0] left,
    output reg [6:0] right 
);
always@(*)begin
    if(in >= 7'd0 && in <= 7'd9)begin
        left = 7'd0;
        right = in ;
    end
    else if(in >= 7'd10 && in <= 7'd19)begin
        left = 7'd1;
        right = in - 7'd10 ;
    end
    else if(in >= 7'd20 && in <= 7'd29)begin
        left = 7'd2;
        right = in - 7'd20 ;
    end
    else if(in >= 7'd30 && in <= 7'd39)begin
        left = 7'd3;
        right = in - 7'd30 ;
    end
    else if(in >= 7'd40 && in <= 7'd49)begin
        left = 7'd4;
        right = in - 7'd40 ;
    end
    else if(in >= 7'd50 && in <= 7'd59)begin
        left = 7'd5;
        right = in - 7'd50 ;
    end
    else if(in >= 7'd60 && in <= 7'd69)begin
        left = 7'd6;
        right = in - 7'd60 ;
    end
    else begin
        left = 7'd0;
        right = 7'd0;
    end
end

endmodule