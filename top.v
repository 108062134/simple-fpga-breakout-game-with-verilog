module top(
  input clk,
  input rst,
  input ran,
  input q_mode,
  inout PS2_CLK,
  inout PS2_DATA,
  output [3:0] vgaRed,
  output [3:0] vgaGreen,
  output [3:0] vgaBlue,
  output hsync,
  output vsync,
  output [2:0] cur_flag,
  output [3:0] sel,
  output [6:0] display
);

`define A 8'h1C  // left
`define D 8'h23  // right
`define W 8'h1D  // up
`define X 8'h22  // down
`define space 8'h29   // space
`define velocity 10'd25

wire clk_25MHz;
wire valid;
wire [9:0] h_cnt; //640
wire [9:0] v_cnt;  //480


wire left_signal,right_siganl;
wire clk_div_100HZ;
wire clk_1sec;

reg [9:0] length_tmp;
wire [9:0] length;
wire [9:0] ball_x,ball_y;
reg [3:0] game_time;
wire [3:0] game_time_tmp;
wire [2:0] game_state;
wire [9:0] reward;
wire [49:0] map;
Freq_div_1hz f3(clk,clk_1sec);
assign length = 10'd50;

assign game_time_tmp = (clk_1sec == 1'b1 ? (game_time + 4'd1):game_time);


clock_divisor clk_wiz_0_inst(
  .clk(clk),
  .clk1(clk_25MHz)
);

q_agent q_ (
  .clk(clk),
  .rst(rst),
  .q_mode(q_mode),
  .reward(reward),
  .left_signal(left_signal),
  .right_signal(right_signal),
  .game_state(game_state),
  .map(map),
  .cur_flag(cur_flag),
  .ball_x(ball_x),
  .ball_y(ball_y)
);

game_engine game(
  .clk(clk),
  .rst(rst),
  .ran(ran),
  .q_mode(q_mode),
  .left_signal(left_signal),
  .right_signal(right_signal),
  .h_cnt(h_cnt),
  .v_cnt(v_cnt),
  .valid(valid),
  .vgaRed(vgaRed),
  .vgaGreen(vgaGreen),
  .vgaBlue(vgaBlue),
  .cur_flag(cur_flag),
  .sel(sel),
  .display(display),
  .length(length),
  .PS2_CLK(PS2_CLK),
  .PS2_DATA(PS2_DATA),
  .reward(reward),
  .map(map),
  .game_state(game_state),
  .ball_x(ball_x),
  .ball_y(ball_y)
);

vga_controller vga_inst(
  .pclk(clk_25MHz),
  .reset(rst),
  .hsync(hsync),
  .vsync(vsync),
  .valid(valid),
  .h_cnt(h_cnt),
  .v_cnt(v_cnt)
);


always@(posedge clk)begin
  if(rst == 1'b1)begin
    game_time <= 4'd0;
  end
  else begin
    game_time <= game_time_tmp;
  end
end

/*
always@(*)begin
  if(game_time == 4'd15)begin
    if(length > 10'd2)begin
      length_tmp = length - 10'd8;
    end
    else begin
      length_tmp = length;
    end
  end
  else begin
    length_tmp = length;
  end
end
*/


      
endmodule
