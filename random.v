`timescale 1ps/1ps

module Random(
    input clk,
    output reg [49:0] random =50'd82308084
);

reg [49:0] nxt_random;
wire tmp;

xor Xor(tmp,random[43],random[41],random[37],random[29],random[8],random[0]);

always@(*)begin
    nxt_random = {tmp,random[49:1]};
end

always@(posedge clk)begin
    random <= nxt_random;
end

endmodule