`timescale 1ps/1ps
module Freq_div_for_q (
    input clk,
    output reg div_clk
);

reg [26:0] cnt = 27'd0;
reg [26:0] tmp_cnt;

always @(posedge clk) begin
    if(cnt[25] == 1'b1) div_clk <= 1'b1;
    else div_clk <= 1'b0;
end

always@* 
tmp_cnt = cnt + 27'd1;

always@(posedge clk)begin
    cnt <= tmp_cnt;
end
    
endmodule