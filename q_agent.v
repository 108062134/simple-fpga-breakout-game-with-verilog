`timescale 1ps/1ps

module q_agent(
    input clk,
    input rst,
    input q_mode,
    input [2:0] cur_flag,
    input [9:0] ball_x,
    input [9:0] ball_y,
    input [9:0] reward,
    input [49:0] map,
    input [2:0] game_state,  // 0 => gaming 1 => lose 2 => win
    output reg left_signal,
    output reg right_signal
);

`define learning_rate 10'd1   // 0.01 alpha
`define reward_decay 10'd9    // 0.9  gamma
`define e_greedy 10'd9        // 0.9  epsilon
/*
always@(*) begin
    left_signal = 1'b0;
    right_signal = 1'b0;
end
*/
reg write_enable;
wire [29:0] m;
reg left_signal_nxt = 1'b0;
reg right_signal_nxt = 1'b0;
reg [9:0] position_x,position_x_nxt;
assign m = {map[49:20]};

reg [9:0] q_table [9:0][9:0][29:0][1:0];  // position_x , ball_x , m , action
wire [9:0] random_or_not;
wire [9:0] random_action;

Random_10bits R1(clk,random_or_not);
Random_10bits R2(clk,random_action);

reg [9:0] pre_position_x, pre_position_x_nxt;
reg [9:0] pre_ball_x,pre_ball_x_nxt;
reg [29:0] pre_m, pre_m_nxt;
reg [1:0] pre_action, pre_action_nxt;

reg [4:0] state , nxt_state;
wire clk_q;
Freq_div_for_q ff(clk,clk_q);

always@(*)begin
    if(cur_flag == 3'd3 || cur_flag == 3'd4)begin
        if(ball_y == 10'd240)begin
            position_x_nxt = ball_x;
        end
        else begin
            position_x_nxt = position_x;
        end
    end
    else begin
        position_x_nxt = position_x;
    end
end

reg [9:0] q_target;
 

always@(posedge clk_q)begin
    if(rst == 1'b1 || q_mode == 1'b0)begin
        position_x <= 10'd10;

        pre_action <= 2'd0;
        pre_m <= m;
        pre_ball_x <= 10'd10;
        pre_position_x <= 10'd10;
    end
    else begin   // position_x 瑼Ｘ
        position_x <= position_x_nxt;

        pre_action <= pre_action_nxt;
        pre_m <= pre_m_nxt;
        pre_ball_x <= pre_ball_x_nxt;
        pre_position_x <= pre_position_x_nxt;
        if(write_enable == 1'b1)begin
            q_table[pre_position_x][pre_ball_x][pre_m][pre_action] <= 
            (10'd3* (q_table[pre_position_x][pre_ball_x][pre_m][pre_action]))/10'd10 + q_target;
        end
        else begin   // pre is initial 
            q_table[pre_position_x][pre_ball_x][pre_m][pre_action] <= 
            10'd0;
        end
        //else
    end
end


always@(*)begin             // choose action
    if(game_state == 3'd0 && q_mode == 1'b1)begin   // gaming
        if(random_or_not < 10'd960)begin
            if( ( (q_table[position_x][ball_x][m][2'd0]) >  (q_table[position_x][ball_x][m][2'd1])) &&
                ( (q_table[position_x][ball_x][m][2'd0]) >  (q_table[position_x][ball_x][m][2'd2])) )begin  // hold
                left_signal = 1'b0;
                right_signal = 1'b0;
                pre_action_nxt = 2'd0;
            end
            else if( ( (q_table[position_x][ball_x][m][2'd1]) >  (q_table[position_x][ball_x][m][2'd0])) &&
                ( (q_table[position_x][ball_x][m][2'd1]) >  (q_table[position_x][ball_x][m][2'd2])) )begin // right
                left_signal = 1'b0;
                right_signal = 1'b1;
                pre_action_nxt = 2'd1;
            end
            else begin     // left
                left_signal = 1'b1;
                right_signal = 1'b0;
                pre_action_nxt = 2'd2;
            end
        end
        else begin      // choose random action
            if(random_action < 10'd333)begin
                left_signal = 1'b1;
                right_signal = 1'b0;
                pre_action_nxt = 2'd2;
            end
            else if(random_action < 10'd666 && random_action >= 10'd333 )begin
                left_signal = 1'b0;
                right_signal = 1'b1;
                pre_action_nxt = 2'd1;
            end
            else begin
                left_signal = 1'b0;
                right_signal = 1'b0;
                pre_action_nxt = 2'd0;
            end
        end
    end
    else begin
        left_signal = 1'b0;
        right_signal = 1'b0;
        pre_action_nxt = 2'd0;
    end
end


always @(*)begin           // learning 
    if(game_state == 3'd0 && q_mode == 1'b1)begin   // gaming
        if(pre_ball_x != 10'd10 && pre_action != 2'd0 && pre_position_x != 10'd10)begin
            if(( (q_table[position_x][ball_x][m][0]) >  (q_table[position_x][ball_x][m][1])) &&
                ( (q_table[position_x][ball_x][m][0]) >  (q_table[position_x][ball_x][m][2])) )begin
                q_target = 10'd7*(10'd50 +  (q_table[position_x][ball_x][m][0]))/10'd10 ;
            end
            else if(( (q_table[position_x][ball_x][m][1]) >  (q_table[position_x][ball_x][m][0])) &&
                ( (q_table[position_x][ball_x][m][1]) >  (q_table[position_x][ball_x][m][2])))begin
                q_target = 10'd7*(10'd50 +  (q_table[position_x][ball_x][m][1]))/10'd10 ;
            end
            else begin
                q_target = 10'd7*(10'd50 +  (q_table[position_x][ball_x][m][2]))/10'd10 ;
            end
            write_enable = 1'b1;
        end 
        else begin
            write_enable = 1'b0;
            q_target = 10'd0;
        end
        pre_position_x_nxt =  position_x;
        pre_ball_x_nxt = ball_x;
        pre_m_nxt = m;
    end
    else begin   //  dead or win 
        if(pre_ball_x != 10'd10 && pre_action != 2'd0 && pre_position_x != 10'd10)begin
            write_enable = 1'b1;
            if(((q_table[position_x][ball_x][m][0]) >  (q_table[position_x][ball_x][m][1])) &&
                ( (q_table[position_x][ball_x][m][0]) >  (q_table[position_x][ball_x][m][2])) )begin
                q_target = 10'd7*( (q_table[position_x][ball_x][m][0]) - 10'd300)/10'd10;
            end
            else if(( (q_table[position_x][ball_x][m][1]) >  (q_table[position_x][ball_x][m][0])) &&
                ( (q_table[position_x][ball_x][m][1]) >  (q_table[position_x][ball_x][m][2])))begin
                q_target = 10'd7*( (q_table[position_x][ball_x][m][1]) - 10'd300)/10'd10;
            end
            else begin
                q_target = 10'd7*( (q_table[position_x][ball_x][m][2]) - 10'd300)/10'd10;
            end
        end
        else begin
            write_enable = 1'b0;
            q_target = 10'd0;
        end
        pre_m_nxt = m;
        pre_ball_x_nxt = 10'd10;
        pre_position_x_nxt = 10'd10;
    end
end

endmodule