`timescale 1ps/1ps

module sev_segment(
    input [6:0] num,
    output reg [6:0] display
);
always@(*)begin
    case(num)
    7'd0:display = 7'b0000001;
    7'd1:display = 7'b1001111;
    7'd2:display = 7'b0010010;
    7'd3:display = 7'b0000110;
    7'd4:display = 7'b1001100;
    7'd5:display = 7'b0100100;
    7'd6:display = 7'b0100000;
    7'd7:display = 7'b0001111;
    7'd8:display = 7'b0000000;
    7'd9:display = 7'b0000100;
    default:display = 7'b0000001;
    endcase
end

endmodule